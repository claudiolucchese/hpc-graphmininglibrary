# GraphMiningLibrary

Starting point for HPC Course

## Command line compilation instructions

To compile the project, execute the following commands in the main source folder.

    mkdir build
    cd build
    cmake -DCMAKE_BUILD_TYPE=Release .
    make

For any subsequent compilation it is sufficient to run the `make` command within the `build` folder.